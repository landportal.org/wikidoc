# How to ingest statistical data

# Creation of RDF files (observations)

- Clone the repository [Excel2RDF](https://gitlab.com/landportal.org/excel2rdf.git)
- Take the folder [Simple_XLS_Importer](https://gitlab.com/landportal.org/excel2rdf/-/tree/main/Simple_XLS_Importer) as main reference
- Run the bash script run_new.sh to transform the excel file in rdf graphs
- Remarks: this tool uses python2 
   
# Ingestion in LandPortal

## Create a new dataset

- Go to [https://dev.landportal.org](https://dev.landportal.org/) and from the **Dashboard** select **Content** —> **Taxonomy** —> **Datasets** —> **Add term**

![Schermata 2022-01-26 alle 12.15.19.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.15.19.png)

- Fill the form with the available information about the dataset. The minimum fields to be filled are:
    - Name: To fill with the name of the dataset
    - Type of Data: for example, “Statistical”
    - Dataset description: short description about the dataset
    - Dataset provider
    - ID: To fill with the ID of the dataset
    - Related aside: Add **LBVC:Download Indicator**
- Click on “Save”
- From the Dashboard click on “Datasets” **Content** —> **Taxonomy** —> **Datasets** to have the list of all the available datasets: Search for the one you have created and click on it.

![Schermata 2022-01-26 alle 12.29.14.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.29.14.png)

- From the dataset page click on the “Edit” icon (Blue pencil)
    
    ![Schermata 2022-01-26 alle 12.31.35.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.31.35.png)
    
- From the browser modify the URL as follows to obtain the .rdf file of the dataset
    
    from [https://dev.landportal.org/taxonomy/term/XXXX/edit](https://dev.landportal.org/taxonomy/term/8968/edit) to [https://dev.landportal.org/taxonomy_term/](https://dev.landportal.org/taxonomy/term/8968/edit)XXXX.rdf
    

![Schermata 2022-01-26 alle 12.33.51.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.33.51.png)

- Press the “Return” button on your keyboard to download the .rdf file
- Go to [https://devvirtuoso.landportal.org/conductor/main_tabs.vspx?logout=1&sid=4faef26e1ee29afb35bbb66ca7813cff&realm=virtuoso_admin](https://devvirtuoso.landportal.org/conductor/main_tabs.vspx?logout=1&sid=4faef26e1ee29afb35bbb66ca7813cff&realm=virtuoso_admin) and access with your credentials
- Click on **Linked Data** —> **Quad Store Upload**

![Schermata 2022-01-26 alle 12.37.54.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.37.54.png)

- In the “Named Graph IRI” write [**http://datasets.landportal.info**](http://datasets.landportal.info/)
- Click on “Select File” to select your dataset .rdf file
- Click on “**Upload**”

```xml
PREFIX cex: <http://purl.org/weso/ontology/computex#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX qb:   <http://purl.org/linked-data/cube#>

SELECT DISTINCT
?datasetID
COUNT(DISTINCT ?indicator) AS ?nIndicators
COUNT(?obs) AS ?nObs
COUNT(DISTINCT ?country) AS ?nCountryWithValue
FROM <http://data.landportal.info>
FROM <http://datasets.landportal.info>
WHERE{
   ?obs qb:dataSet ?datasetURI ;
   	cex:ref-indicator ?indicator;
   	cex:ref-area ?country ;
   	cex:value ?value.
   ?datasetURI skos:notation ?datasetID.
 
} ORDER BY ?datasetID
```

## Create a new indicators

- Go to [https://dev.landportal.org](https://dev.landportal.org/) and from the **Dashboard** select **Content** —> **Taxonomy** —> **Indicators** —> **Add term**
    
    ![Schermata 2022-01-27 alle 10.11.59.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.11.59.png)
    
- Fill the form with the available information about the indicator. More specifically
    - Dataset: Select from the list the dataset the indicator belongs to.
    - Indicator name: the title/name of the indicator
    - Indicator description: fill with the description of the indicator
    - ID: fill with the ID of the indicator. To retrieve the ID of the indicator open the .rdf with the observations and search for the entity `cex:ref-indicator` . It contains the full URI of the ID as follows
        
        ```xml
         <cex:ref-indicator rdf:resource="http://data.landportal.info/indicator/UN-UN-AG_LND_FRSTCHG"/>
        ```
        
        You have to copy the last part of the URI after the **indicator/** and paste it into the ID fill in the form**.** In this case `UN-UN-AG_LND_FRSTCHG`
        
- Measurement unit: fill with the measurement unit of the indicator. For example “Percentage”
- Related aside: Add LBVC:Download indicator
- **Click on SAVE**
- Reopen again the indicator in edit mode
- From the Dashboard click on “Datasets” **Content** —> **Taxonomy** —> **Indicators** to have the list of all the available indicators: Search for the one you have created and click on it.
    
    ![Schermata 2022-01-27 alle 10.36.35.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.36.35.png)
    
- From the dataset page click on the “Edit” icon (Blue pencil)
    
    ![Schermata 2022-01-27 alle 10.37.27.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.37.27.png)
    
- The edit form appears again. Go to the **Land Book Indicators** section and click on “**Add another  item**”
- Fill all the elements according to the below Figure.

![Schermata 2022-01-27 alle 10.38.15.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.38.15.png)

- Click on **Save** at the bottom of the page
- From the Dashboard click on “Datasets” **Content** —> **Taxonomy** —> **Indicators** to have the list of all the available indicators: Search for the one you have created and click on it.
    
    ![Schermata 2022-01-27 alle 10.36.35.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.36.35.png)
    
- From the dataset page click on the “Edit” icon (Blue pencil)
    
    ![Schermata 2022-01-27 alle 10.37.27.png](How%20to%20ing%204755a/Schermata_2022-01-27_alle_10.37.27.png)
    
- From the browser modify the URL as follows to obtain the .rdf file of the dataset
    
    from [https://dev.landportal.org/taxonomy/term/XXXX/edit](https://dev.landportal.org/taxonomy/term/8968/edit) to [https://dev.landportal.org/taxonomy_term/](https://dev.landportal.org/taxonomy/term/8968/edit)XXXX.rdf
    
- Press the “Return” button on your keyboard to download the .rdf file
- Go to [https://devvirtuoso.landportal.org/conductor/main_tabs.vspx?logout=1&sid=4faef26e1ee29afb35bbb66ca7813cff&realm=virtuoso_admin](https://devvirtuoso.landportal.org/conductor/main_tabs.vspx?logout=1&sid=4faef26e1ee29afb35bbb66ca7813cff&realm=virtuoso_admin) and access with your credentials
- Click on **Linked Data** —> **Quad Store Upload**

![Schermata 2022-01-26 alle 12.37.54.png](How%20to%20ing%204755a/Schermata_2022-01-26_alle_12.37.54.png)

- In the “Named Graph IRI” write [http://indicators.landportal.info](http://indicators.landportal.info/)
- Click on “Select File” to select your dataset .rdf file
- Click on “**Upload**”
- Log out and Log in again in virtuoso
- Go to [https://dev.landportal.org/sparql](https://dev.landportal.org/sparql) and type the following SPARQL query to check if the rdf graph of the indicator has been correctly imported
    
     
    
    ```xml
    	PREFIX cex: <http://purl.org/weso/ontology/computex#>
    	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    	PREFIX dct: <http://purl.org/dc/terms/>
    	
    	SELECT *
    	FROM <http://indicators.landportal.info>
    	WHERE {
    	?indicatorUrl a cex:Indicator ;
    	  skos:notation ?ID;
    	  rdfs:label ?label ;
    		dct:description ?description .
    	}
    	ORDER BY ?label
    ```
    
- If not, repeat the upload in virtuoso