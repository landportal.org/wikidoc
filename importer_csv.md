
# Creation of the dataset

There are two scenarios to define a bibliographic dataset



1. There is an API that makes data available
2. The dataset is created from scratch via a web crawler using data available on individual web pages

In both cases the dataset must be formalized in CSV using the following structure. **All fields in the table are considered mandatory**


## Compulsory CSV structure


<table>
  <tr>
   <td>FIELD
   </td>
   <td>Description
   </td>
   <td>Format
   </td>
   <td>Automatically generated
   </td>
  </tr>
  <tr>
   <td>ID
   </td>
   <td>Unique ID of the bibliographic resource. Normally SBN or DOI code
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Title
   </td>
   <td>The title of the resource 
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Pages
   </td>
   <td>The pages of the resource
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Authors
   </td>
   <td>The authors of the resource
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Abstract/Description
   </td>
   <td>The abstract of the resource
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Geographical focus
   </td>
   <td>The geographical focus of the resource
   </td>
   <td>Country name must be provided according to value in the <strong>“Country name” </strong>col of this <a href="https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes">wikipedia</a> list
   </td>
   <td>The country name is converted into the corresponding ISO code. The input source MUST refer to the list in <a href="https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes">wikipedia</a> 
   </td>
  </tr>
  <tr>
   <td>Copyright details 
   </td>
   <td>The copyright of the resource
   </td>
   <td>Copyright must be provided according to value in the following list
<ul>

<li>Open Data Commons Attribution License

<li>Open Data Commons Public Domain Dedication and License

<li>Creative Commons Zero (Public Domain)

<li>Creative Commons Attribution-NoDerivs

<li>Creative Commons Attribution

<li>Creative Commons Attribution-NonCommercial

<li>Creative Commons Attribution-NonCommercial-NoDerivs

<li>Creative Commons Attribution-ShareAlike

<li>Creative Commons Attribution-NonCommercial-ShareAlike

<li>All rights reserved
</li>
</ul>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>License
   </td>
   <td>The license of the resource
   </td>
   <td>Copyright must be provided according to value in the following list
<ul>

<li>Open Data Commons Attribution License

<li>Open Data Commons Public Domain Dedication and License

<li>Creative Commons Zero (Public Domain)

<li>Creative Commons Attribution-NoDerivs

<li>Creative Commons Attribution

<li>Creative Commons Attribution-NonCommercial

<li>Creative Commons Attribution-NonCommercial-NoDerivs

<li>Creative Commons Attribution-ShareAlike

<li>Creative Commons Attribution-NonCommercial-ShareAlike

<li>All rights reserved
</li>
</ul>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Publication date
   </td>
   <td>The date of the publication of the resource
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Related concepts
   </td>
   <td>The related concepts of the resource
   </td>
   <td>Provided according to the values in <a href="https://landvoc.org">LandVoc</a> vocabulary
<p>
<strong>Multiple concepts must be defined using  semicolon and space as follows “; “ (land governance; activities)</strong>
   </td>
   <td>YES
   </td>
  </tr>
  <tr>
   <td>Themes
   </td>
   <td>The Themes of the resource
   </td>
   <td>
<ul>

<li>Forest Landscape Restoration

<li>Land & Corruption

<li>Land & Investments

<li>Land Stakeholders & Institutions

<li>Socio-Economic & Institutional Context

<li>Forest Tenure

<li>Land & Food Security

<li>Land Conflicts

<li>Land, Climate Change & Environment

<li>Urban Tenure

<li>Indigenous & Community Land rights

<li>Land &  Gender

<li>Land in post-confict settings

<li>Rangelands, Drylands & Pastoralism
Multiple themes can be used. Multiple themes <strong>MUST </strong>be separated by <strong>semicolon “ ; “</strong>
</li>
</ul>
   </td>
   <td>YES
   </td>
  </tr>
  <tr>
   <td>Publishers
   </td>
   <td>The publisher of the resource
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Languages
   </td>
   <td>The language of the resource 
   </td>
   <td><strong>MUST </strong>be formatted as <a href="https://www.loc.gov/standards/iso639-2/php/code_list.php">ISO 639-1</a> 
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Link to the publication
   </td>
   <td>The link to the pdf file
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Link to the original website
   </td>
   <td>The link to the publication’s page 
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>



## Importer - Dataset Creation Workflow -


### Scenario A

Data is available via API. in this case, a script must be implemented to make a request to the endpoint API to select the necessary elements and download them in CSV format (i.e JSON to CSV). This task is normally done using a  [Python request](https://realpython.com/python-requests/). 


### Scenario B

There is no API and the dataset must be created from scratch using the data made available by the content provider through the portal web pages. In this case, the crawler already used for this process can be taken as a reference.

The crawler exports the data in a CSV format according to the scheme shown in the table above.

The scripts are available here:[ https://gitlab.com/landportal.org/scraper](https://gitlab.com/landportal.org/scraper)

The crawler must be adapted each time according to the structure of the source portal. 

As a reference architecture must be taken as a reference this [script](https://gitlab.com/landportal.org/scraper/-/blob/master/JPS/jps.py) where are already defined the FOR LOOP necessary to get the data from a top level page and its subpages through the href.

To configure the development environment follow the instructions in the [README](https://gitlab.com/landportal.org/scraper/-/blob/master/README.md) file.


#### Remarks Scenario A & B

In both Use case A and Use case B resources are materialized in a .csv file maintaining the table structure described above. The Themes should not be considered (even if present in the source page) in this first phase since **they are automatically created.**


### Themes

The column Themes is defined separately as themes are automatically filled using the FastText library from Facebook. To how to configure and install the tool please refer to the [official documentation](https://fasttext.cc/).

Landportal already provides a [repository](https://gitlab.com/landportal.org/themes_classification) with the currently used configuration. 


### Workflow



* After cloning the repository and having configured the library, create a new csv file with a column named **“Abstract/Description”** filled with the data of the columns Abstract or Description on the resulting .csv file from the **Scenario A/B.**
* Train the model running the script **[train.py](https://gitlab.com/landportal.org/themes_classification/-/blob/main/train.py)** . A file named **themes_landvoc.bin** will be created.
* Run the script **[predict.py](https://gitlab.com/landportal.org/themes_classification/-/blob/main/predict.py)**
* A new file **[themes_predicted.csv](https://gitlab.com/landportal.org/themes_classification/-/blob/main/themes_predicted.csv)** will be created
* Use the values from the col **prediction **to fill the col **Themes ** in the main source csv file **  **


#### Remarks Themes

The values from the col **prediction **must be normalized. To do so:



* Remove **('__label__**
* Remove **'__label__**
* Remove **‘)**
* Substitute **‘,** with **;**
* Substitute the remaining “_” with “ “ (blank space)

**[Take this file as main example](https://gitlab.com/landportal.org/scraper/-/blob/master/LIFT/LIFT_importer_final.csv)**

**To have the working dataset the col Themes must be integrated into the main dataset. The final CSV must be formatted with the separator “;” instead of “,”. **

 


# Ingestion of the dataset

To create a new importer, the following steps must be followed: 



* Create a new user
* Create/clone an importer
* Upload the CSV file to the importer


## Create a new user

From the Dashboard click on **People → Add User**




![alt_text](images/image1.png "image_tooltip")


Then fill all the information needed


## Create/clone an importer

From the Dashboard click on **Structure → Feeds Importer. **The following page with the list of all importers will appear




![alt_text](images/image2.png "image_tooltip")


Take the “resource_equity” importer as the main schema and click on clone. While cloning remember to change the name of the importer and to click on “Clone Tamper Plugins”



![alt_text](images/image3.png "image_tooltip")


Once created go back to the **Structure → Feeds Importer **to have the full list of the importers. Go to the new importer and click on **edit**


![alt_text](images/image4.png "image_tooltip")


Select CSV as parser



![alt_text](images/image5.png "image_tooltip")


Then click on the **Tamper. **Go to the username section and edit the user name changing the name with the user name of the previously created account.

 


![alt_text](images/image6.png "image_tooltip")



## Upload the CSV file to the importer

To upload the final csv file into the importer from the dashboard go to **Content → import. ** Then click on the importer which was created.




![alt_text](images/image7.png "image_tooltip")



![alt_text](images/image8.png "image_tooltip")


Change the delimiter from “,” to “;” then import the csv file.


## Check the results

To check the results from the Dashboard click on **People. **Filter the user according the username.




![alt_text](images/image9.png "image_tooltip")


Click on the username to have the corresponding importer.
